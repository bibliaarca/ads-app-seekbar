package ifsc.edu.br.appseekbar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    SeekBar seekBar;
    TextView  textView;
    NumberFormat formatacaoPercentual = NumberFormat.getPercentInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        seekBar =(SeekBar) findViewById(R.id.seekbar);
        textView = (TextView) findViewById(R.id.text);
        seekBar.setOnSeekBarChangeListener(seekBarChangeListener);

    }


    private final SeekBar.OnSeekBarChangeListener seekBarChangeListener=  new SeekBar.OnSeekBarChangeListener() {
            @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            sincronizaTextView();
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };



    public  void sincronizaTextView(){
        /*
        * De alguma forma não tive tempo de entender porque ele conta de 0 a 10
        * */
        int valor= seekBar.getProgress();
        double d = (double) valor * 10;
        if (this.textView != null) {
            this.textView.setText(formatacaoPercentual.format(d));
        }
    }
}

